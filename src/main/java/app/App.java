package app;

import lib.Library;

public class App {
    private Library lib1;

    public App() {
        lib1 = new Library();   
    }

    public Library getLibrary() {
        return lib1;
    }

    public static void main(String[] args) {
        App app = new App();
        System.out.println(
            "Library version: " + app.getLibrary().getVersion()
        );
    }
}
